<?
/**[N]**
 * JIBAS Education Community
 * Jaringan Informasi Bersama Antar Sekolah
 * 
 * @version: 3.7 (Maret 12, 2015)
 * @notes: JIBAS Education Community will be managed by Yayasan Indonesia Membaca (http://www.indonesiamembaca.net)
 * 
 * Copyright (C) 2009 Yayasan Indonesia Membaca (http://www.indonesiamembaca.net)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 **[N]**/ ?>
<?
require_once('include/mainconfig.php');
require_once('include/db_functions.php');

// --- Patch Management Framework ---
require_once('include/global.patch.manager.php');
ApplyGlobalPatch(".");

// --- LiveUpdate Status ----
session_name("jbsmain");
session_start();

$lid = -1; // current liveupdate id
$dbconnect = @mysql_connect($db_host, $db_user, $db_pass);
if ($dbconnect)
{
	$dbselect = @mysql_select_db("jbsclient", $dbconnect);
	
	if ($dbselect)
	{
		$sql = "SELECT nilai FROM jbsclient.liveupdateconfig WHERE tipe='MIN_UPDATE_ID'";
		$result = @mysql_query($sql, $dbconnect);
		$row = @mysql_fetch_row($result);
		$minid = $row[0];
		
		$sql = "SELECT MAX(liveupdateid) FROM jbsclient.liveupdate";
		$result = @mysql_query($sql, $dbconnect);
		$row = @mysql_fetch_row($result);
		$maxinstalled  = is_null($row[0]) ? 0 : $row[0];
		
		$lid = $minid >= $maxinstalled ? $minid : $maxinstalled;
	}
	
	@mysql_close($dbconnect);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS Education Community <?=$G_VERSION?></title>
<link href="script/vtip.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="images/jibas2015.ico" />
<script language="javascript" src="script/jquery.min.js"></script>
<script language="javascript" src="script/ajax.js"></script>
<script language="javascript" src="script/vtip.js"></script>
<script language="javascript" src="index.js"></script>
<link rel="stylesheet" href="script/bgstretcher.css" />
<script language="javascript" src="script/bgstretcher.js"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 8]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js ie9 oldie" lang="en"><![endif]-->
<link rel="stylesheet" media="all" href="style/bootstrap.min.css"/>
<link rel="stylesheet" media="all" href="style/style-new.css"/>
<link rel="stylesheet" media="all" href="style/landing.css"/>
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
</head>
 <body>
        <header>
            <span><img src="images/yhb-logo.png" alt="logo"/></span>
            <div class="header-text">
                <h1>sistem informasi sekolah (sisko)</h1>
                <h3>Yayasan Harapan Bangsa - SD ASY-SYIFA 1</h3>
            </div>
        </header>
        <div class="landing-shape"></div>

        <div class="menu">
            <ul>
             	<li>
                    <a href="akademik/index.php" class="infoSiswa">
                        <span><i class="fa fa-clipboard"></i></span><br/>
                        akademik
                    </a>
                </li>
                <li>
                    <a href="anjungan/index.php" class="infoSiswa">
                        <span><i class="fa fa-file-o"></i></span><br/>
                        anjungan
                    </a>
                </li>
                <li>
                    <a href="infoguru/index.php" class="infoGuru">
                        <span><i class="fa fa-mortar-board"></i></span><br/>
                        info guru
                    </a>
                </li>
                <li>
                    <a href="infosiswa/index.php" class="infoSiswa">
                        <span><i class="fa fa-users"></i></span><br/>
                        info siswa
                    </a>
                </li>
                <li>
                    <a href="http://yhb-bandung.com/admin/sdasysyifa1/finance" class="infoSiswa">
                        <span><i class="fa fa-dollar"></i></span><br/>
                        keuangan
                    </a>
                </li>
                <li>
                    <a href="http://yhb-bandung.com/admin/sdasysyifa1/login" class="infoSiswa">
                        <span><i class="fa fa-globe"></i></span><br/>
                        website
                    </a>
                </li>
            </ul>
        </div>
        
        <script type="text/javascript" src="script/jquery-1.9.0.min.js"></script>
        <script type="text/javascript" src="script/bootstrap.min.js"></script>
    </body> 
</html>

<? if (!$_SESSION['lugetstatus'] || $_SESSION['lugetlid'] != $lid) { ?>
<script type="text/javascript" language="javascript">
getLuStatus(<?=$lid?>);
</script>
<? } ?>
