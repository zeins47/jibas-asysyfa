<?
/**[N]**
 * JIBAS Education Community
 * Jaringan Informasi Bersama Antar Sekolah
 * 
 * @version: 3.7 (Maret 12, 2015)
 * @notes: JIBAS Education Community will be managed by Yayasan Indonesia Membaca (http://www.indonesiamembaca.net)
 * 
 * Copyright (C) 2009 Yayasan Indonesia Membaca (http://www.indonesiamembaca.net)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 **[N]**/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
YHB - SD ASYSYFA 1 - INFO GURU
</title>
<link href="images/jibas2015.ico" rel="shortcut icon" />
<link rel="stylesheet" type="text/css" href="style/style.css">
<script type="text/javascript" language="javascript" src='../script/jquery.min.js'></script>
<script type="text/javascript" language="javascript" src="../script/footer.js"></script>
<script language="JavaScript" src="script/resizing_background.js"></script>
<link rel="stylesheet" href="../script/bgstretcher.css" />
<script language="javascript" src="../script/bgstretcher.js"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 8]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js ie9 oldie" lang="en"><![endif]-->
<link rel="stylesheet" media="all" href="../style/bootstrap.min.css"/>
<link rel="stylesheet" media="all" href="../style/style-new.css"/>
<link rel="stylesheet" media="all" href="../style/landing.css"/>
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
<script language="JavaScript">
function cek_form() 
{
	var user,pass;
	user = document.form.username.value;
	pass = document.form.password.value;
	if(user.length == 0 || pass.length == 0) 
	{
		alert("Anda harus mengisi username dan password sebelum masuk ke dalam sistem!");
		document.form.username.focus();
		return false;
	}
	return true;
}


function alertSize() {
  var WinHeight = 0;
  var WinWidth = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    WinHeight = window.innerHeight;
	WinWidth = window.innerWidth;
  } else if( document.documentElement &&
      ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    WinHeight = document.documentElement.clientHeight;
	WinWidth = document.documentElement.clientWidth;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    WinHeight = document.body.clientHeight;
	WinWidth = document.body.clientWidth;
  }
  document.getElementById('Main').style.left = (parseInt(WinWidth)/2-200)+"px";
  document.getElementById('Main').style.top = (parseInt(WinHeight)/2-80)+"px";

}

function InputHover(txt,id,state){
	var x = document.getElementById(id).value;
	if (state=='1'){
		if (x==txt){
			document.getElementById(id).value='';
			document.getElementById(id).style.color='#000';
		} else {
			document.getElementById(id).style.color='#000';			
		}
	} else {
		if (x==txt || x==''){
			document.getElementById(id).style.color='#636363';
			document.getElementById(id).value=txt;
		} else {
			document.getElementById(id).style.color='#000';
		}
	}
}

function ChgInputPass(s,d,status){
	var Vs = document.getElementById(s);
	var Vd = document.getElementById(d);
	if (status=='1')
	{
		Vs.style.display='none';
		Vd.style.display='block';
		document.getElementById(d).focus();
	} else {
		if (Vd.value.length==0){
			Vs.style.display='block';
			Vd.style.display='none';
		} else {
			Vs.style.display='none';
			Vd.style.display='block';
		}
	}
}

// $(document).ready(function () {
//     $(document).bgStretcher({
//         images: ['../images/background07.jpg'], imageWidth: 1680, imageHeight: 1050
//     });
// });
</script>

</head>
<body class="login" onload="alertSize(); document.getElementById('username').focus()" onresize="alertSize()">
  <h2>sistem informasi <span>info guru</span></h2>
    <div class="login-box">
	<form method="post" name="form" id="form" action="redirect.php" onSubmit="return cek_form()">
      <h3><span>login</span> here please...</h3>
      <input type="text" name="username" id="username" placeholder="Username"/>
       <input name="passwordfake" id="passwordsfake" value="Password" onfocus="ChgInputPass('passwordsfake','passwords','1')" type="text"    />
   		 <input name="password" id="passwords" style="display:none;" value="" onblur="ChgInputPass('passwordsfake','passwords','0')"  type="password"    />
      <input type="submit" value="login"/>
  </form>
  </div>
    <p><a href="http://sd1.yhb-bandung.com/"><i class="fa fa-hand-o-left"></i>Kembali ke Menu Utama</a></p>

<script type="text/javascript" src="../script/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="../script/bootstrap.min.js"></script>
</body>
</html>